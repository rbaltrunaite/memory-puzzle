"use strict";

const cards = [
    {image: 1, key: 1},
    {image: 2, key: 2},
    {image: 3, key: 3},
    {image: 4, key: 4},
    {image: 5, key: 5},
    {image: 6, key: 6},
    {image: 7, key: 7},
    {image: 8, key: 8},
    {image: 9, key: 9},
    {image: 10, key: 10}
];

const cardsCopy = [...cards];

const both = cards.concat(cardsCopy);

function shuffle(both) {
    both.sort(() => Math.random() - 0.5);
}

shuffle(both);

let num;

const board = document.getElementById("board");

for (num = 0; num < both.length; num++) {
    board.innerHTML +=
        '<div class="card" data-status="hidden" data-match="false" data-key="' + both[num].key + '">' +
            '<div class="front"></div>' +
            '<div class="back">' +
                '<img src="images/' + both[num].image + '.jpg" alt="card"/>' +
            '</div>' +
        '</div>';
}

$(".card").flip({
    trigger: 'manual'
});

let clickCount = 0;
let firstGuess;
let secondGuess;
let firstGuessKey;
let secondGuessKey;
let pairs = 0;


$(".card").on('click', function () {

    if ($(this).attr('data-status') !== 'visible') {

        console.log($(this).attr('data-status'));
        $(this).attr('data-status', 'visible');
        $(this).flip('true');

        clickCount++;
        if (clickCount < 3) {

            if (clickCount === 1) {
                firstGuess = $(this);
                firstGuessKey = $(this).data('key');
                console.log(firstGuess);
            } else {
                secondGuess = $(this);
                secondGuessKey = $(this).data('key');
                console.log(secondGuess);
            }

            console.log([firstGuessKey, secondGuessKey]);
            if (firstGuessKey !== undefined && secondGuessKey !== undefined) {
                if (firstGuessKey === secondGuessKey) {
                    match(firstGuess, secondGuess, reset);
                } else {
                    reset(firstGuess, secondGuess);
                }
            }
        }
    }
});

const match = (firstGuess, secondGuess, callback) => {
    firstGuess.children().addClass('matchedImage');
    secondGuess.children().addClass('matchedImage');
    callback(firstGuess, secondGuess);
    console.log('match');
    firstGuess.attr('data-match', 'true');
    secondGuess.attr('data-match', 'true');
    firstGuess.attr('data-status', 'visible');
    secondGuess.attr('data-status', 'visible');
    console.log('status changed ' + firstGuess.data('status'));
    pairs++;
    console.log('pairs: ' + pairs);
    setTimeout(alertFunc, 1200);
    function alertFunc() {
        if (pairs === 10)
        {alert("You won! Congratulations!");}
    }
};

const reset = (firstGuess, secondGuess) => {

    firstGuess.attr('data-status', 'hidden');
    secondGuess.attr('data-status', 'hidden');

    setTimeout( () => {
        $(".card[data-match='false']").flip(false);
    }, 1200);

    firstGuess = undefined;
    secondGuess = undefined;
    firstGuessKey = undefined;
    secondGuessKey = undefined;
    clickCount = 0;

    console.log('reset');
};
